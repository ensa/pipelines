#!/bin/bash

#***************************************************************#
#                            gatk.sh                            #
#                  written by Kerensa McElroy                   #
#                        June 26th, 2015                        #
#                                                               #
#               runs gatk pipeline on bam files                 #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}
export OUTDIR=$WORK/analysis/$PROJECT/gatk/${REF%.*}
export REFERENCE=$WORK/data/${PROJECT}/$REF

#----------------------------------------------------------------#

module load parallel
module load jdk

export REGION=all

#if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi

    
mkdir -p $OUTDIR/$REGION/gVCFs
#mkdir -p $OUTDIR/$REGION/bamout

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log

java -Xmx1g -jar ~/bin/GenomeAnalysisTK.jar --version >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/gatk.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

function call_hap {
    SAMPLE=$1

    java -Xmx15g -jar ~/bin/GenomeAnalysisTK.jar \
        -T HaplotypeCaller \
        -R $REFERENCE \
        -I $INDIR/${SAMPLE}*realigned.bam \
        -ploidy $PLOIDY \
        --emitRefConfidence GVCF \
        --variant_index_type LINEAR \
        --variant_index_parameter 128000 \
        -o $OUTDIR/$REGION/gVCFs/${SAMPLE}.raw.snps.indels.g.vcf &> $OUTDIR/$REGION/gVCFs/${SAMPLE}.log
#        -L $INTERVAL 2> $OUTDIR/$REGION/gVCFs/${STEM}.log
        #-mmq 5 \
        #-bamout $OUTDIR/$REGION/bamout/${STEM}_realigned_${REGION}_hc.bam 2>> ~/$PROJECT/logs/main_$TODAY.log
}

export -f call_hap 

parallel -j ${CORES} call_hap ::: $SAMP_NAMES

