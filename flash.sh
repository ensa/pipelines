#!/bin/bash

#***************************************************************#
#                        flash.sh                               #
#                  written by Kerensa McElroy                   #
#                       May 22, 2017                            #
#                                                               #
#                 run flash on fastq files                      #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working

export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=$WORK/analysis/$PROJECT/flash/exp1

#-----------------------software variables----------------------#
M=125
x=0.01

#---------------------------------------------------------------#

module load parallel
module load jdk
module load flash

mkdir -p $OUTDIR 

cat ~/scripts/flash.sh >> ~/$PROJECT/logs/main_${TODAY}.log

function merge {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/${UNIQUE}//"`
    
    flash $INDIR/$STEM*p.fq -M 125 -x 0.01 -o $STEM -d $OUTDIR -z -t 1 2>&1 | tee $OUTDIR/$STEM_flash.log
}

export -f merge

ls ${INDIR}/${SUBSET}*${READ_ONE}*p.fq | parallel -j ${CORES} merge
