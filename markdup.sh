#!/bin/bash
#***************************************************************#
#                          markdup.sh                           #
#                  written by Kerensa McElroy                   #
#                         June 18, 2015                         #
#                                                               #
#        runs picard mark duplicates on sorted bam files        #
#                   also fixes mate information                 #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export SAMPLES=~/${PROJECT}/${PROJECT}.samples
export INDIR=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}
#----------------------------------------------------------------#

module load parallel/20141122
module load samtools/0.1.19
module load jdk

echo "Picard version:" >> ~/$PROJECT/logs/main_$TODAY.log

java -jar -Xmx2g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar MarkDuplicates --version 2>> ~/$PROJECT/logs/main_$TODAY.log

echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/markdup.sh >> ~/$PROJECT/logs/main_$TODAY.log

echo '' >> ~/$PROJECT/logs/main_$TODAY.log

function markdups {
    export SAMPLE=$1
    files=`grep "${SAMPLE}" ${SAMPLES} | cut -f1 | sed "s/${UNIQUE}/_fixmate_sort.bam/" | sort -u | sed "s/^/I=/"`
    cd ${INDIR}

    echo '#!/bin/bash' > ${SAMPLE}.picard
    echo "java -jar -Xmx20g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar MarkDuplicates \\" >> ${SAMPLE}.picard
    echo $files | xargs -n 1 sh -c 'echo "\t${0} \\" >> ${SAMPLE}.picard'
    echo "  O=${SAMPLE}_fixmate_sort_MDUP.bam \\" >> ${SAMPLE}.picard
    echo "  REMOVE_DUPLICATES=false \\" >> ${SAMPLE}.picard
    echo "  METRICS_FILE=${SAMPLE}_MDUP.txt \\" >> ${SAMPLE}.picard
    echo "  MAX_FILE_HANDLES=1000 \\" >> ${SAMPLE}.picard
    echo "  VALIDATION_STRINGENCY=SILENT 2> ${SAMPLE}_mdup.log "    >> ${SAMPLE}.picard

    chmod a+x ${SAMPLE}.picard
    ./${SAMPLE}.picard
    samtools index ${SAMPLE}_fixmate_sort_MDUP.bam #&& rm ${file}*
}


export -f markdups

parallel -j ${CORES} markdups ::: ${SAMP_NAMES}
