#!/bin/bash

#***************************************************************#
#                            gatk.sh                            #
#                  written by Kerensa McElroy                   #
#                          June 18, 2015                        #
#                                                               #
#               runs gatk's genotype gvfcsfiles                 #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/gatk/${REF%.*}

#----------------------------------------------------------------#

module load jdk

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log
java -Xmx1g -jar ~/bin/GenomeAnalysisTK.jar --version >> ~/$PROJECT/logs/main_$TODAY.log

#export REGION=autosomal

#if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi

export REGION=all

echo $INDIR/$REGION/gVCFs/*idx | sed 's/.idx */\n/g' > $INDIR/${PROJECT}_${REGION}_new_gvcf.list


java -Xmx100g -jar ~/bin/GenomeAnalysisTK.jar \
    -T GenotypeGVCFs \
    -R $WORK/data/$PROJECT/$REF \
    --variant $INDIR/${PROJECT}_${REGION}_new_gvcf.list \
    -o $INDIR/${REGION}/$PROJECT_${REGION}.new.all.gvcf.raw.vcf \
#    -L $INTERVAL \
    -dcov $DOWNSAMPLE 2> $INDIR/${REGION}/$PROJECT_${REGION}.new.all.gvcf.raw.log

