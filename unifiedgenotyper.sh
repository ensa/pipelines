#!/bin/bash

#***************************************************************#
#                       unifiedgenotyper.sh                     #
#                  written by Kerensa McElroy                   #
#                          June 22, 2015                        #
#                                                               #
#           runs gatk's unified genotyper on bam files          #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/${ALIGNER}
export OUTDIR=$WORK/analysis/$PROJECT/gatk
export REFERENCE=$WORK/data/$PROJECT/$REF

#----------------------------------------------------------------#

module load parallel
module load gatk/3.3.0
module load jdk

export REGION=autosomal

if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi

mkdir -p $OUTDIR/$REGION/vcfs

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log
GenomeAnalysisTK --version >> ~/$PROJECT/logs/main_$TODAY.log

ls ${INDIR}/${REGION}/${SUBSET}*.bam > ${OUTDIR}/${REGION}/vcfs/${PROJECT}_${REGION}_bam.list

function run_ugt {
    java -Xmx100g -jar /apps/gatk/3.3.0/GenomeAnalysisTK.jar \
        -T UnifiedGenotyper \
        -R $REFERENCE \
        -I ${OUTDIR}/${REGION}/vcfs/${PROJECT}_${REGION}_bam.list \
        -ploidy $PLOIDY \
        -o $OUTDIR/$REGION/vcfs/${PROJECT}_${REGION}.ugt.raw.vcf \
        -L $INTERVAL \
        -glm BOTH \
        --max_alternate_alleles 10 2>> ~/$PROJECT/logs/main_$TODAY.log
}

export -f run_ugt 

run_ugt
