#!/bin/bash

#***************************************************************#
#                       replaceContigs.sh                       #
#                  written by Kerensa McElroy                   #
#                       September 3, 2015                       #
#                                                               #
#           replace problematic characters in bam file          #
#***************************************************************#

#------------------------project variables----------------------#
cd ~/working
export WORK=`pwd -P`
export INPUT=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}

#---------------------------------------------------------------#

module add samtools/0.1.19
module add parallel/20141122

samtools | head -4 >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

cat ~/scripts/replaceContigs.sh >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

function run_cont {
    file=$1
    STEM=`echo $file|sed "s/.bam$//"`
    samtools view -@ 1 $file > ${STEM}.sam
    sed -i 's/lcl|/lcl_/g' ${STEM}.sam
    sed -i 's/:1-/_1-/g' ${STEM}.sam
    samtools view -bS -@ 1 ${STEM}.sam > ${STEM}_mod.bam 
    samtools index ${STEM}_mod.bam && rm ${STEM}.bam* ${STEM}.sam
}

export -f run_cont

ls ${INPUT}/${SUBSET}*.bam | parallel -j ${CORES} run_cont
