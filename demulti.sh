#!/bin/bash

#***************************************************************#
#                            demulti.py                         #
#                  written by Kerensa McElroy                   #
#                        October 23, 2015                       #
#                                                               #
# demultiplexes dual indexed nextera data with no mismatches    #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/data/$PROJECT
export INDEXES=$WORK/data/$PROJECT/$INDEX
#---------------------------------------------------------------#

cat ~/scripts/demulti.sh >> ~/${PROJECT}/logs/main_${TODAY}.log
echo '' >> ~/${PROJECT}/logs/main_${TODAY}.log

export SPECIES_LIST=`grep '' ${INDEXES} | cut -f2 | sort -u`

for species in ${SPECIES_LIST}; do
    index1=`grep $species ${INDEXES} | cut -f4`
    index2=`grep $species ${INDEXES} | cut -f6`
    count=1
    count=`zgrep -c "${index1}+${index2}" ${INDIR}/${SUBSET}*${READ_ONE}*`
    echo $count
    zgrep -A 3 "${index1}+${index2}" ${INDIR}/${SUBSET}*${READ_ONE}* | grep -vwE '\-\-' > ${INDIR}/${species}_${index1}_${index2}_${count}_R1.fastq
    zgrep -A 3 "${index1}+${index2}" ${INDIR}/${SUBSET}*${READ_TWO}* | grep -vwE '\-\-' > ${INDIR}/${species}_${index1}_${index2}_${count}_R2.fastq
done





