#!/bin/bash

#***************************************************************#
#                        trimmomatic.sh                         #
#                  written by Kerensa McElroy                   #
#                       January 20, 2015                        #
#                                                               #
#                 run trimmomatic on fastq files                #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working

export WORK=`pwd -P`
export INDIR=$WORK/data/$PROJECT
export OUTDIR=$WORK/analysis/$PROJECT/trimmed
export ADAPTERS=~/adapters

#-----------------------software variables----------------------#
export TRIMMOMATIC=/apps/trimmomatic/0.32/trimmomatic-0.32.jar

#---------------------------------------------------------------#

module load parallel
module load jdk

mkdir -p $OUTDIR 

cat ~/scripts/trimmomatic.sh >> ~/$PROJECT/logs/main_${TODAY}.log

function trim {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/${UNIQUE}//"`
    ADAPT_SEQ=`echo $STEM|sed "s/.*$ADAPT_LEFT//;s/$ADAPT_RIGHT.*//"`
    ADAPT=`echo ${ADAPTERS}/${ADAPT_TYPE}*${ADAPT_SEQ}*.fa`
    
    java -jar $TRIMMOMATIC PE \
        -threads 1 \
        -phred${PHRED} \
        -trimlog ${OUTDIR}/${STEM}_log.txt \
        ${INDIR}/${IN_PATH}/${STEM}*${EXT} \
        ${OUTDIR}/${STEM}_R1_p.fq \
        ${OUTDIR}/${STEM}_R1_u.fq \
        ${OUTDIR}/${STEM}_R2_p.fq \
        ${OUTDIR}/${STEM}_R2_u.fq \
        ILLUMINACLIP:${ADAPT}:${SEEDMISMATCH}:${PALINCLIP}:${SIMPLECLIP}:${MINADAPTLEN}:${KEEPREADS} \
        SLIDINGWINDOW:${WINDOWSIZE}:${BASEQUAL} \
        LEADING:${BASEQUAL} \
        TRAILING:${BASEQUAL} \
        MINLEN:${MINLENGTH} > ${OUTDIR}/${STEM}.txt 2>&1
}

export -f trim

ls ${INDIR}/${IN_PATH}/${SUBSET}*${READ_ONE}*${EXT}| parallel -j ${CORES} trim
