#/bin/bash

#***************************************************************#
#                        iter_consensus.sh                      #
#                  written by Kerensa McElroy                   #
#                        September 14, 2015                     #
#                                                               #
#                  merges bam files by sample                   #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples
export BAMIN=$WORK/analysis/$PROJECT/bwa/${REF%.*}
#---------------------------------------------------------------#


module add samtools/0.1.19
module add parallel/20141122
module add jdk


function run_merge {
    export SUB=$1
    files=`grep "${SUB}" ${SAMPLES} | cut -f1 | sort`
    echo '#/bin/bash' > ${BAMIN}/${SUB}.picard
    echo "java -jar -Xmx100g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar MergeSamFiles \\" >> ${BAMIN}/${SUB}.picard
    echo -e "\tVALIDATION_STRINGENCY=SILENT \\" >> ${BAMIN}/${SUB}.picard
    echo $files | xargs -n 2 sh -c 'echo "\tINPUT=${BAMIN}/${1%%${UNIQUE%.*}*}${BAMEXT}_fixmate_sort.bam \\" >> "${BAMIN}/${SUB}.picard"'
    echo -e "\tOUTPUT=${BAMIN}/${SUB}${BAMEXT}_merge.bam \\" >> ${BAMIN}/${SUB}.picard
    echo -e "\tASSUME_SORTED=true" >> ${BAMIN}/${SUB}.picard
    chmod a+x ${BAMIN}/${SUB}.picard
    ${BAMIN}/${SUB}.picard
    samtools index ${BAMIN}/${SUB}${BAMEXT}_merge.bam 

    java -jar -Xmx10g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.126/picard.jar AddOrReplaceReadGroups \
        VALIDATION_STRINGENCY=SILENT \
        I=${BAMIN}/${SUB}${BAMEXT}_merge.bam \
        O=${BAMIN}/${SUB}${BAMEXT}.bam \
        RGSM=${SUB} \
        RGLB=NA \
        RGPL=NA \
        RGPU=NA 

    samtools index ${BAMIN}/${SUB}${BAMEXT}.bam && rm ${BAMIN}/${SUB}${BAMEXT}_merge.bam*


export -f run_merge

export SAMP_NAMES=`grep '' ${SAMPLES} | cut -f2 | sort -u`

parallel -j 1 run_merge ::: $SPECIES
