#/bin/bash

#***************************************************************#
#                            mitobim.sh                         #
#                  written by Kerensa McElroy                   #
#                          July 28, 2015                        #
#                                                               #
#       assemble full mitochondrial genomes using mitobim       #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=${WORK}/analysis/$PROJECT/mitobim/${REF%.*}
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module add mitobim/1.8  
module add mira/4.0.2
module add samtools/0.1.19
module add parallel/20141122

mkdir -p ${INDIR}/MT

cat ~/scripts/mitobim.sh >> ~/${PROJECT}/logs/main_${TODAY}.log
echo '' >> ~/${PROJECT}/logs/main_${TODAY}.log
#samtools faidx $WORK/data/$PROJECT/$REF $MITO > $WORK/data/$PROJECT/MT_${REF}

function my_mito {
    SAMPLE=$1
    files=`grep "${SAMPLE}" ${SAMPLES} | cut -f1`
    echo $files | xargs -n 2 sh -c 'interleave.py ${INDIR}/${1%%${UNIQUE%.*}*}*_p.fq ${INDIR}/${2%%{UNIQUE%.*}*}*_p.fq > ${INDIR}/MT/${0}_${1%%${UNIQUE%.*}*}_R2_interleaved.fq' ${SAMPLE}
    cat ${INDIR}/MT/${SAMPLE}* > ${INDIR}/MT/interleaved_${SAMPLE}.fastq && rm ${INDIR}/MT/${SAMPLE}*
    head -66445688 ${INDIR}/MT/interleaved_${SAMPLE}.fastq > ${INDIR}/MT/interleaved_${SAMPLE}_sub.fastq && rm ${INDIR}/MT/interleaved_${SAMPLE}.fastq
    rm -r ${OUTDIR}/${SAMPLE}
    mkdir -p ${OUTDIR}/${SAMPLE}
    cd ${OUTDIR}/${SAMPLE}
    cmd="MITObim --denovo --paired -end 500 -sample $SAMPLE -ref MT_${REF%%.*} -readpool ${INDIR}/MT/interleaved_${SAMPLE}_sub.fastq --clean --quick $WORK/data/$PROJECT/MT_${REF}"
    $cmd > ${OUTDIR}/${SAMPLE}/mitobim_${TODAY}.log 2>&1
    rm -r /tmp/${SAMPLE}*
    rm ${INDIR}/MT/inter*${SAMPLE}*
}


export -f my_mito

export SAMP_NAMES='Miles188' 
#export SAMP_NAMES=`grep '' ${SAMPLES} | cut -f2 | sort -u`

parallel -j 1 my_mito ::: $SAMP_NAMES

#my_mito $SAMP_NAMES
