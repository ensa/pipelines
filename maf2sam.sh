#/bin/bash

#***************************************************************#
#                            maf2sam.sh                         #
#                  written by Kerensa McElroy                   #
#                          August 14, 2015                      #
#                                                               #
#       get mitobim assembled genome in bam format              #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=${WORK}/analysis/$PROJECT/mitobim/
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples
export s_start='MITObim has reached a stationary read number after'
export s_end=' iterations!!'
#---------------------------------------------------------------#

module add samtools/0.1.19
module add parallel/20141122
module add python

cat ~/scripts/maf2sam.sh >> ~/${PROJECT}/logs/main_${TODAY}.log
echo '' >> ~/${PROJECT}/logs/main_${TODAY}.log

function run_maf2sam {

    SAMPLE=$1
    if [ "$REF" == '' ]
    then
        ref=`grep "${SAMPLE}" ${SAMPLES} | cut -f6 | sort -u`
    else
        ref=$REF
    fi

    iterations=`grep "${s_start}" ${INDIR}/${ref%.*}/${SAMPLE}/mitobim*log`
    iterations=`echo ${iterations#${s_start}}`
    iterations=`echo ${iterations%${s_end}}`
    iter_folder=`echo iteration${iterations}`
    maf2sam.py ${INDIR}/${ref%.*}/${SAMPLE}/${iter_folder}/*assembly/*results/*AllStrains.unpadded.fasta ${INDIR}/${ref%.*}/${SAMPLE}/${iter_folder}/*assembly/*results/*maf > ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}.sam

    samtools view -bS ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}.sam > ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}.bam && rm ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}.sam
    samtools sort ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}.bam ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}_sort && rm ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}.bam
    samtools index ${INDIR}/${ref%.*}/$SAMPLE/${SAMPLE}_AllStrains_i${iterations}_sort.bam
    
    SAMP_REF=$(basename ${INDIR}/${ref%.*}/${SAMPLE}/${iter_folder}/*assembly/*results/*${SAMPLE}.unpadded.fasta) 
    cp ${INDIR}/${ref%.*}/${SAMPLE}/${iter_folder}/*assembly/*results/*${SAMPLE}.unpadded.fasta ${INDIR}/${ref%.*}/${SAMPLE}/${SAMP_REF%%.*}_i${iterations}.unpadded.fasta

}

export -f run_maf2sam

parallel -j $CORES run_maf2sam ::: $SAMP_NAMES

