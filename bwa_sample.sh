#!/bin/bash

#***************************************************************#
#                            bwa.sh                             #
#                  written by Kerensa McElroy                   #
#                       March 30, 2016                          #
#                                                               #
#         run bwa on trimmed fastq files, by sample             #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=$WORK/analysis/$PROJECT/bwa/${REF%.*}
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module load bwa
module load parallel/20141122

mkdir -p $OUTDIR

bwa | head -5 >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/bwa.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >>  ~/$PROJECT/logs/main_$TODAY.log

# make reference index files 

if ${MAKE_INDEX}; then bwa index -a bwtsw -p $WORK/data/$PROJECT/${REF%.*} \ 
    $WORK/data/$PROJECT/$REF 2>> ~/$PROJECT/logs/main_$TODAY.log; fi


# run bwa on all trimmed fastq files

function run_bwa {
    sample=$1
    files=`grep "${sample}" ${SAMPLES} | cut -f1 | sed "s/$UNIQUE//" | sort -u`
    for STEM in ${files}; do
        LIBRARY=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f3` 
        CENTRE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f4`
        SEQDATE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f5`
        UNIT=`gzip -cd ${WORK}/data/$PROJECT/${STEM}*${READ_ONE}*gz | head -1 | cut -d':' -f${UNIT_RX}`    
        ID=`echo ${UNIT:1}_${sample} | sed s/:/_/g`
        echo "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
             `"DT:${SEQDATE}\tLB:${sample}_${LIBRARY}\t"` \
             `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${sample}" >> $OUTDIR/${sample}_${STEM}.log   
        bwa mem $WORK/data/$PROJECT/${REF%.*} $INDIR/$STEM*p.fq* \
            -t 4 \
            -k $SEED \
            -w $WIDTH \
            -r $INTERNAL \
            -T 50 \
            -M \
            -R "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
               `"DT:${SEQDATE}\tLB:${sample}_${LIBRARY}\t"` \
               `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${sample}" \
            > ${OUTDIR}/${sample}_${STEM}.sam 2> $OUTDIR/${sample}_${STEM}.log; done

}

export -f run_bwa 

parallel -j ${CORES} run_bwa ::: ${SAMP_NAMES}
