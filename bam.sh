#!/bin/bash

#***************************************************************#
#                            bam.sh                             #
#                  written by Kerensa McElroy                   #
#                       January 20, 2015                        #
#                                                               #
#       convert sam files to indexed, sorted bam files          #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}
export BAM_MEM=10G    # max memory per thread

#---------------------------------------------------------------#

module add samtools/1.3.0
module add parallel/20141122

samtools | head -4 >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

cat ~/scripts/bam.sh >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

function run_bam {
    NAME=$1
    STEM=`echo $NAME|sed "s/${UNIQUE}//"`
    cd $INDIR
    samtools fixmate -O bam ${STEM}*sam ${STEM}_fixmate.bam #&& rm ${STEM}*sam
    samtools sort -O bam -@ 1 -m ${BAM_MEM} -o ${STEM}_fixmate_sort.bam ${STEM}_fixmate.bam #&& rm ${STEM}_fixmate.bam
    samtools index ${STEM}_fixmate_sort.bam
}

export -f run_bam

parallel -j ${CORES} run_bam ::: ${FILES} 
