#!/bin/bash

#***************************************************************#
#                      mito_sam_consensus.sh                    #
#                  written by Kerensa McElroy                   #
#                       March 30, 2016                          #
#                                                               #
#       use samtools / bcftools to make consensus from bam      #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/${PROJECT}/mitobim
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples
#---------------------------------------------------------------#

module add samtools/0.1.19
module add parallel/20141122

samtools | head -4 >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

cat ~/scripts/mito_sam_consensus.sh >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

function run_cons {
    SAMPLE=$1
    files=`grep "${SAMPLE}" ${SAMPLES} | cut -f1 | sed "s/$UNIQUE/*bam/" | sed "s/^/*/" | sort -u`

    if [ "$REF" == '' ]
    then
        ref=`grep "${SAMPLE}" ${SAMPLES} | cut -f6 | sort -u`
    else
        ref=$REF
    fi

    REFERENCE=`echo ${INDIR}/${ref%.*}/${SAMPLE}/${SAMPLE}*trim.fasta`

    cd $INDIR/${ref%.*}/${SAMPLE}
    echo ${SAMPLE} ${PLOIDY} > ${SAMPLE}.txt

    samtools mpileup -ug -q 50 -Q 15 --ff 3840 -f ${REFERENCE} ${files} | bcftools view -cg -s ${SAMPLE}.txt - | ~/bin/vcfutils.pl vcf2fq > ${SAMPLE}.fq
    fastqToFasta.py ${SAMPLE}.fq -l 1700 -w 60
}

export -f run_cons

parallel -j ${CORES} run_cons ::: ${SAMP_NAMES}

