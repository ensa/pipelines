#/bin/bash

#***************************************************************#
#                            maf2sam.sh                         #
#                  written by Kerensa McElroy                   #
#                          August 15, 2016                      #
#                                                               #
#            trim up mitogenome made using mitobim              #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=${WORK}/analysis/$PROJECT/mitobim
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples
#---------------------------------------------------------------#

module add parallel/20141122
module add python

cat ~/scripts/mito_trim.sh >> ~/${PROJECT}/logs/main_${TODAY}.log
echo '' >> ~/${PROJECT}/logs/main_${TODAY}.log



function run_trim {

    SAMPLE=$1
    if [ "$REF" == '' ]
    then
        ref=`grep "${SAMPLE}" ${SAMPLES} | cut -f6 | sort -u`
    else
        ref=$REF
    fi
    
    cd ${INDIR}/${REF%.*}/${SAMPLE}

    FILE=${SAMPLE}-*unpadded.fasta
    
    cut_mito.py $FILE $BEGIN $END
}

export -f run_trim

parallel -j $CORES run_trim ::: $SAMP_NAMES

