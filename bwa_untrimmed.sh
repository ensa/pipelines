#!/bin/bash

#***************************************************************#
#                            bwa.sh                             #
#                  written by Kerensa McElroy                   #
#                       January 20, 2015                        #
#                                                               #
#                 run bwa on untrimmed fastq files                #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/data/$PROJECT
export OUTDIR=$WORK/analysis/$PROJECT/bwa_untrimmed/${REF%.*}
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module load bwa
module load parallel/20141122

mkdir -p $OUTDIR

bwa | head -5 >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/bwa_untrimmed.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >>  ~/$PROJECT/logs/main_$TODAY.log

# make reference index files 

#bwa index -a bwtsw -p $WORK/data/$PROJECT/${REF%.*}  $WORK/data/$PROJECT/$REF \
#    2>> ~/$PROJECT/logs/main_$TODAY.log

# run bwa on all untrimmed fastq files

function run_bwa {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/${UNIQUE}//"`
    SAMPLE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f2`
    LIBRARY=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f3` 
    CENTRE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f4`
    SEQDATE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f5`
    UNIT=`gzip -cd $file | head -1 | cut -d':' -f${UNIT_RX}`    
    ID=`echo ${UNIT:1}_${SAMPLE} | sed s/:/_/g`
    echo "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
           `"DT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\t"` \
           `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" >> $OUTDIR/${STEM}.log   
    bwa mem $WORK/data/$PROJECT/${REF%.*} $INDIR/$IN_PATH/$STEM*.fq* \
        -t 1 \
        -k $SEED \
        -w $WIDTH \
        -r $INTERNAL \
        -M \
        -R "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
           `"DT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\t"` \
           `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" \
        > ${OUTDIR}/${STEM}.sam 2> $OUTDIR/${STEM}.log
}

export -f run_bwa 

ls ${INDIR}/${IN_PATH}/${SUBSET}*${READ_ONE}*.fq* | parallel -j ${CORES} run_bwa 
