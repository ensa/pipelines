#!/bin/bash

#***************************************************************#
#                            fastqc.sh                          #
#                  written by Kerensa McElroy                   #
#                         April 10, 2015                        #
#                                                               #
#             runs fastqc on gzipped raw data files             #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=$WORK/analysis/$PROJECT/fastqc

#----------------------------------------------------------------#

module add fastqc
module add parallel

mkdir -p $OUTDIR

cat ~/scripts/fastqc_trimmed.sh >> ~/$PROJECT/logs/main_${TODAY}.log

fastqc -v >> ~/$PROJECT/logs/main_${TODAY}.log

function run_fastqc {
    file=$1
    CMD="fastqc $file --noextract -f fastq -o $OUTDIR"
    echo -e "\n$CMD" >> ~/${PROJECT}/logs/main_$TODAY.log
    $CMD
}

export -f run_fastqc

ls ${INDIR}/${SUBSET}*p.fq | parallel -j ${CORES} run_fastqc 
