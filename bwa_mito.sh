#!/bin/bash

#***************************************************************#
#                            bwa_mito.sh                        #
#                  written by Kerensa McElroy                   #
#                       August 22, 2016                         #
#                                                               #
#                 run bwa on mitochondrial reference            #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=$WORK/analysis/$PROJECT/mitobim/${REF%.*}
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module load bwa
module load parallel/20141122
module add samtools/0.1.19
mkdir -p $OUTDIR

bwa | head -5 >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/bwa_mito.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >>  ~/$PROJECT/logs/main_$TODAY.log

# make reference index files 

bwa index -a is -p $WORK/data/$PROJECT/${REF%.*} $WORK/data/$PROJECT/$REF \
    2>> ~/$PROJECT/logs/main_$TODAY.log


# run bwa on all trimmed fastq files

function run_bwa {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/${UNIQUE}//"`
    SAMPLE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f2`
    SPECIES=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f6`
    LIBRARY=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f3` 
    CENTRE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f4`
    SEQDATE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f5`
    UNIT=`head -1 ${INDIR}/${STEM}*${READ_ONE}*p.fq | cut -d':' -f${UNIT_RX}` #`gzip -cd $file | head -1 | cut -d':' -f${UNIT_RX}`    
    ID=`echo ${UNIT:1}_${SAMPLE}_${SPECIES} | sed s/:/_/g`
    echo "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
           `"DT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\t"` \
           `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" > $OUTDIR/${SAMPLE}/${STEM}.log   
    if [ ! -f ${OUTDIR}/${SAMPLE}/${STEM}.sam ]
    then
        mkdir ${OUTDIR}/${SAMPLE}
        bwa mem $WORK/data/$PROJECT/${REF%.*} ${INDIR}/${STEM}*p.fq \
            -t $THREADS \
            -k $SEED \
            -w $WIDTH \
            -r $INTERNAL \
            -T $SCORE \
            -M \
            -R "@RG\tID:${ID}\tCN:${CENTRE}\tDT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\tPL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" > ${OUTDIR}/${SAMPLE}/${STEM}.sam 2>> $OUTDIR/${SAMPLE}/${STEM}.log
        samtools view -bS ${OUTDIR}/${SAMPLE}/${STEM}.sam > ${OUTDIR}/${SAMPLE}/${STEM}.bam
        samtools sort ${OUTDIR}/${SAMPLE}/${STEM}.bam ${OUTDIR}/${SAMPLE}/${STEM}_sort
        echo ${SAMPLE} ${PLOIDY} > ${OUTDIR}/${SAMPLE}/${SAMPLE}.txt
        samtools mpileup -ug -q 50 -Q 15 --ff 3840 -f $WORK/data/$PROJECT/${REF} ${files} | bcftools view -cg -s ${SAMPLE}.txt - | ~/bin/vcfutils.pl vcf2fq > ${SAMPLE}.fq
        
    fi
}

export -f run_bwa 

parallel -j ${CORES} run_bwa ::: ${FILES}
