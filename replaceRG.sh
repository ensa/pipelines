#!/bin/bash

#***************************************************************#
#                            replace.sh                         #
#                  written by Kerensa McElroy                   #
#                       January 22, 2015                        #
#                                                               #
#            replace readgroups on broken bam files             #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/data/$PROJECT/$IN_PATH
export OUTDIR=$WORK/analysis/$PROJECT/$ALIGNER
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module load parallel/20141122
module load jdk

function replace {
    file=$1
    NAME=$(basename "${file}")
    STEM1=`echo $NAME|sed "s/${MODIFIED}.*//"`
    STEM2=`echo $file|sed "s/.bam$//"`
    SAMPLE=`grep "${STEM1}*${READ_ONE}" $SAMPLES | cut -f2`
    LIBRARY=`grep "${STEM1}*${READ_ONE}" $SAMPLES | cut -f3`
    CENTRE=`grep "${STEM1}*${READ_ONE}" $SAMPLES | cut -f4`
    SEQDATE=`grep "${STEM1}*${READ_ONE}" $SAMPLES | cut -f5`
    UNIT=`gzip -cd ${INDIR}/${SUBSET}*${READ_ONE}*${EXT} | head -1 | cut -d':' -f${UNIT_RX}`
    ID=`echo ${UNIT:1}_${SAMPLE} | sed s/:/_/g`
 
    java -jar -Xmx2g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.126/picard.jar AddOrReplaceReadGroups \
        VALIDATION_STRINGENCY=SILENT \
        I=$file \
        O=${STEM2}_rg.bam \
        RGID=$ID \
        RGCN=$CENTRE \
        RGDT=$SEQDATE \
        RGLB=$LIBRARY \
        RGPL=$PLATFORM \
        RGPU=$UNIT \
        RGSM=$SAMPLE && mv ${STEM2}_rg.bam $file

}

export -f replace 

ls ${OUTDIR}/${SUBSET}*${MODIFIED}.bam | parallel -j ${CORES} replace
