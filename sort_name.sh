#!/bin/bash

#***************************************************************#
#                            sort_name.sh                       #
#                  written by Kerensa McElroy                   #
#                       May 25, 2016                            #
#                                                               #
#                      sort bam files by name                   #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}
export BAM_MEM=10G    # max memory per thread

#---------------------------------------------------------------#

module add samtools/1.3.0
module add parallel/20141122


function sort {
    NAME=$1
    STEM=`echo $NAME|sed "s/bam//"`
    cd $INDIR/mapped
    samtools fixmate -O bam ${STEM}*sam ${STEM}_fixmate.bam && rm ${STEM}*sam
    samtools sort -O bam -@ 1 -m ${BAM_MEM} -o ${STEM}_fixmate_sort.bam ${STEM}_fixmate.bam && rm ${STEM}_fixmate.bam
    samtools index ${STEM}_fixmate_sort.bam
}

export -f run_bam

parallel -j ${CORES} sort ::: ${FILES} 
