#!/bin/bash

#***************************************************************#
#                            mito_bwa.sh                        #
#                  written by Kerensa McElroy                   #
#                       August 14th, 2015                       #
#                                                               #
#     align trimmed reads back to reconstructed mitogenomes     #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=$WORK/analysis/$PROJECT/mitobim
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module add bwa
module add samtools/0.1.19
module add parallel/20141122


bwa | head -5 >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

samtools | head -4 >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

cat ~/scripts/mito_bwa.sh >> ~/$PROJECT/logs/main_$TODAY.log

function run_bwa {

    SAMPLE=$1
    
    if [ "$REF" == '' ]
    then
        ref=`grep "${SAMPLE}" ${SAMPLES} | cut -f6 | sort -u`
    else
        ref=$REF
    fi

    REFERENCE=`echo ${OUTDIR}/${ref%.*}/${SAMPLE}/${SAMPLE}*trim.fasta` 
    bwa index -a is -p ${REFERENCE%.*} ${REFERENCE} 2> ${OUTDIR}/${ref%.*}/${SAMPLE}/${SAMPLE}_bwa_{$TODAY}.log

    files=`grep "${SAMPLE}" ${SAMPLES} | cut -f1 | sed "s/$UNIQUE//" | sort -u`

    cd $OUTDIR/${ref%.*}/${SAMPLE}
    
    for STEM in ${files}; do
        LIBRARY=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f3`
        CENTRE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f4`
        SEQDATE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f5`
        UNIT=`gzip -cd ${WORK}/data/$PROJECT/${STEM}*${READ_ONE}*gz | head -1 | cut -d':' -f${UNIT_RX}`
        ID=`echo ${UNIT:1}_${SAMPLE} | sed s/:/_/g`
        echo "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
             `"DT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\t"` \
             `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" > ${SAMPLE}_${STEM}.log
        bwa mem ${REFERENCE%.*} $INDIR/$STEM*p.fq* \
            -t 4 \
            -k $SEED \
            -w $WIDTH \
            -r $INTERNAL \
            -T 50 \
            -M \
            -R "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
               `"DT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\t"` \
               `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" \
            > ${SAMPLE}_${STEM}.sam 2>> ${SAMPLE}_${STEM}.log
        samtools view -bS -F 3844 -@ 1 ${SAMPLE}_${STEM}.sam > ${SAMPLE}_${STEM}.bam && rm ${SAMPLE}_${STEM}.sam 
        samtools sort -@ 1 ${SAMPLE}_${STEM}.bam ${SAMPLE}_${STEM}_sort && rm ${SAMPLE}_${STEM}.bam
        samtools index ${SAMPLE}_${STEM}_sort.bam; done
}

export -f run_bwa 

parallel -j $CORES run_bwa ::: ${SAMP_NAMES}

