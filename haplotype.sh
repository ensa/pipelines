#!/bin/bash

#***************************************************************#
#                         indelRealign.sh                       #
#                  written by Kerensa McElroy                   #
#                        June 26th, 2015                        #
#                                                               #
#               runs gatk pipeline on bam files                 #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/${ALIGNER}
export OUTDIR=$WORK/analysis/$PROJECT/gatk
export REFERENCE=$WORK/data/$PROJECT/$REF

#----------------------------------------------------------------#

module load parallel
module load gatk/3.3.0

export REGION=autosomal

if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi
    
mkdir -p $OUTDIR/$REGION/gVCFs
mkdir -p $OUTDIR/$REGION/bamout

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log
java -Xmx1g -jar /apps/gatk/3.3.0/GenomeAnalysisTK.jar --version >> ~/$PROJECT/logs/main_$TODAY.log

function run_haplo {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/.bam//"`

    java -Xmx10g -jar /apps/gatk/3.3.0/GenomeAnalysisTK.jar \
        -T HaplotypeCaller \
        -R $REFERENCE \
        -I $INDIR/${REGION}/${STEM}_realigned_${REGION}.bam \
        -ploidy $PLOIDY \
        --emitRefConfidence GVCF \
        --variant_index_type LINEAR \
        --variant_index_parameter 128000 \
        -o $OUTDIR/$REGION/gVCFs/${STEM}.raw.snps.indels.g.vcf \
        -bamout $OUTDIR/$REGION/bamout/${STEM}_realigned_${REGION}_hc.bam \
        -L $INTERVAL 2> $OUTDIR/$REGION/bamout/${STEM}_${TODAY}.log
        #-mmq 5 \
}

export -f run_haplo

ls ${INDIR}/${SUBSET}*MDUP.bam| parallel -j ${CORES} run_haplo
