#!/bin/bash

#***************************************************************#
#                           mito_picard.sh                      #
#                  written by Kerensa McElroy                   #
#                       March 29, 2017                          #
#                                                               #
#                 picard analyis of aligned reads to mitogenome #
#***************************************************************#

#-----------------------default variables-----------------------#
export VALIDATION=SILENT       # sam valid? STRICT,LENIENT,SILENT
export METRIC_LEVEL=ALL_READS  # possible options as per sam RG
export WIN=100                 # sliding window size 
export MAX_REC=200000000       # reads stored in RAM

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export SAMPLES=~/${PROJECT}/${PROJECT}.samples
export INDIR=$WORK/analysis/$PROJECT/mitobim/
export TMP=$WORK/temp
#---------------------------------------------------------------#

module load samtools/0.1.19
module load parallel
module add jdk

echo "Picard version:" >> ~/$PROJECT/logs/main_$TODAY.log

java -jar -Xmx1g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar MeanQualityByCycle --version 2>> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/picard.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log


function picard_mito {
    SAMPLE=$1
    echo $SAMPLE
    files=`grep ${SAMPLE} ${SAMPLES} | cut -f1 | sed "s/$UNIQUE//" | sort -u`
    echo $files
    if [ "$REF" == '' ]
    then
        ref=`grep ${SAMPLE} ${SAMPLES} | cut -f6 | sort -u`
    else
        ref=$REF
    fi
    
    REFERENCE=`echo ${INDIR}/${ref%.*}/${SAMPLE}/${SAMPLE}.fasta`

    OUTDIR=${INDIR}/${ref%.*}/picard
    mkdir -p ${OUTDIR}

    IN=${INDIR}/${ref%.*}/${SAMPLE}

    for STEM in ${files}; do

	    java -jar -Xmx15g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar MeanQualityByCycle \
	        VALIDATION_STRINGENCY=$VALIDATION \
	        R=$REFERENCE \
	        INPUT=$IN/${SAMPLE}_${STEM}_sort.bam \
	        OUTPUT=$OUTDIR/${STEM}_MQBC.txt \
	        CHART_OUTPUT=$OUTDIR/${STEM}_MQBC.pdf \
	        TMP_DIR=/OSM/CBR/NRCA_FINCHGENOM/temp/picard.temp \
	        MAX_RECORDS_IN_RAM=$MAX_REC 2> ${OUTDIR}/${STEM}_MQBC.log
	    wait
	
	    java -jar -Xmx15g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar CollectAlignmentSummaryMetrics \
	        VALIDATION_STRINGENCY=$VALIDATION \
	        LEVEL=$METRIC_LEVEL \
	        R=$REFERENCE \
	        INPUT=$IN/${SAMPLE}_${STEM}_sort.bam \
	        OUTPUT=$OUTDIR/${STEM}_CASM.txt \
	        TMP_DIR=$TMP/picard.temp \
	        MAX_RECORDS_IN_RAM=$MAX_REC 2> ${OUTDIR}/${STEM}_CASM.log
	    wait 
	
	    java -jar -Xmx15g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar QualityScoreDistribution \
	        VALIDATION_STRINGENCY=$VALIDATION \
	        R=$REFERENCE \
	        INPUT=$IN/${SAMPLE}_${STEM}_sort.bam \
	        OUTPUT=$OUTDIR/${STEM}_QSD.txt \
	        CHART=$OUTDIR/${STEM}_QSD.pdf \
	        TMP_DIR=$TMP/picard.temp \
	        MAX_RECORDS_IN_RAM=$MAX_REC 
	    wait
	    
	    java -jar -Xmx15g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar CollectInsertSizeMetrics \
	        VALIDATION_STRINGENCY=$VALIDATION \
	        R=$REFERENCE \
	        INPUT=$IN/${SAMPLE}_${STEM}_sort.bam \
	        OUTPUT=$OUTDIR/${STEM}_CISM.txt \
	        HISTOGRAM_FILE=$OUTDIR/${STEM}_CISM.pdf \
	        TMP_DIR=$TMP/picard.temp \
	        MAX_RECORDS_IN_RAM=$MAX_REC 
	    wait
	 
	   java -jar -Xmx15g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar CollectWgsMetrics \
	        VALIDATION_STRINGENCY=$VALIDATION \
	        R=$REFERENCE \
	        INPUT=$IN/${SAMPLE}_${STEM}_sort.bam \
	        OUTPUT=$OUTDIR/${STEM}_WGS.txt \
	        TMP_DIR=$TMP/picard.temp \
	        MAX_RECORDS_IN_RAM=$MAX_REC 2> ${OUTDIR}/${STEM}_WGS.log 
	    wait 
	
	    java -jar -Xmx15g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar CollectGcBiasMetrics \
	        VALIDATION_STRINGENCY=$VALIDATION \
	        R=$REFERENCE \
	        INPUT=$IN/${SAMPLE}_${STEM}_sort.bam \
	        OUTPUT=$OUTDIR/${STEM}_CGcBM.txt \
	        CHART=$OUTDIR/${STEM}_CGcBM.pdf \
	        SUMMARY_OUTPUT=$OUTDIR/${STEM}_CGcBM_summary.txt \
	        WINDOW_SIZE=$WIN \
	        TMP_DIR=$TMP/picard.temp \
	        MAX_RECORDS_IN_RAM=$MAX_REC 
	    wait
	 
	    java -jar -Xmx15g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar EstimateLibraryComplexity \
	        VALIDATION_STRINGENCY=$VALIDATION \
	        R=$REFERENCE \
	        INPUT=$IN/${SAMPLE}_${STEM}_sort.bam \
	        OUTPUT=$OUTDIR/${STEM}_ELC.txt \
	        TMP_DIR=$TMP/picard.temp \
	        MAX_RECORDS_IN_RAM=$MAX_REC; done 
	
	}

export -f picard_mito

parallel -j ${CORES} picard_mito ::: ${SAMP_NAMES}
