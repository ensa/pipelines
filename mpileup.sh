#!/bin/bash

#***************************************************************#
#                            mpileup.sh                         #
#                  written by Kerensa McElroy                   #
#                       September 14, 2015                      #
#                                                               #
#                generate mpileup files from bam                #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}
export OUTDIR=$WORK/analysis/$PROJECT/mpileup/${REF%.*}
export REFERENCE=$WORK/data/$PROJECT/$REF
export BAM_MEM=10G    # max memory per thread

#---------------------------------------------------------------#

module add samtools/0.1.19
module add parallel/20141122

samtools | head -4 >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

cat ~/scripts/mpileup.sh >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

export REGION=autosomal

mkdir -p $OUTDIR

if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi

function run_mpileup {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/.bam$//"`
    samtools mpileup -B -q 20 -f $REFERENCE --ff UNMAP,SECONDARY,QCFAIL,DUP $file > ${OUTDIR}/${STEM}.mpileup 2> ${OUTDIR}/${STEM}_mpileup.log
}

export -f run_mpileup

ls ${INDIR}/${REGION}/*${SUBSET}*.bam | parallel -j ${CORES} run_mpileup
