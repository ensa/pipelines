#!/bin/bash

#***************************************************************#
#                         indelRealign.sh                       #
#                  written by Kerensa McElroy                   #
#                        June 26th, 2015                        #
#                                                               #
#               runs gatk pipeline on bam files                 #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}
export OUTDIR=$WORK/analysis/$PROJECT/gatk/${REF%.*}
export REFERENCE=$WORK/data/${PROJECT}/$REF

#----------------------------------------------------------------#

module load parallel
module load jdk

#export REGION=autosomal

#if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi
#    
mkdir -p $OUTDIR/intervals
#mkdir -p $INDIR/$REGION

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log

java -Xmx1g -jar ~/bin/GenomeAnalysisTK.jar --version >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/indelRealign.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

#java -jar -Xmx2g -Djava.io.tmpdir=/OSM/CBR/NRCA_FINCHGENOM/temp /apps/picard/1.138/picard.jar CreateSequenceDictionary \
#    R=$REFERENCE \
#    O=${REFERENCE%.*}.dict
#

cd $INDIR

function run_indel {

    SAMPLE=$1

    java -Xmx10g -jar ~/bin/GenomeAnalysisTK.jar \
        -T RealignerTargetCreator \
        -I ${SAMPLE}_fixmate_sort_MDUP.bam \
        -R $REFERENCE \
        -o ${OUTDIR}/intervals/${SAMPLE}_targetIntervals.list \
        -dcov $DOWNSAMPLE \
#        $INTERVAL \
        $FIXQUAL &> ${OUTDIR}/intervals/${SAMPLE}_RTC.log 

    java -Xmx10g -jar ~/bin/GenomeAnalysisTK.jar \
        -T IndelRealigner \
        -I ${SAMPLE}_fixmate_sort_MDUP.bam \
        -R $REFERENCE \
        -targetIntervals $OUTDIR/intervals/${SAMPLE}_targetIntervals.list \
        -o $INDIR/${SAMPLE}_fix_mdup_realigned.bam \
        -dcov $DOWNSAMPLE \
#        $INTERVAL \
        $FIXQUAL &> ${OUTDIR}/intervals/${SAMPLE}_IR.log
}

export -f run_indel 

parallel -j ${CORES} run_indel ::: ${SAMP_NAMES}
