#!/bin/bash

#***************************************************************#
#                          consensus.sh                         #
#                  written by Kerensa McElroy                   #
#                          June 19, 2015                        #
#                                                               #
#     generate per-sample consensus fasta sequence from vcf     #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/${PROJECT}/gatk/$INTERVAL/vcfs
export OUTDIR=$WORK/analysis/${PROJECT}/gatk/$INTERVAL/fasta
export REFERENCE=$WORK/data/${PROJECT}/$REF

#----------------------------------------------------------------#

module load parallel
module load gatk/3.3.0

mkdir -p $OUTDIR

export SAMPLES=`grep '#CHROM' ${INDIR}/${PROJECT}_${INTERVAL}.ugt.filtered.all.vcf| cut -d'	' -f10-`

echo 'GATK version:' >> ~/${PROJECT}/logs/main_$TODAY.log
GenomeAnalysisTK --version >> ~/${PROJECT}/logs/main_$TODAY.log

function run_consensus {
    SAMPLE=$1

    GenomeAnalysisTK \
        -T SelectVariants \
        -R $REFERENCE \
        -V ${INDIR}/${PROJECT}_${INTERVAL}.ugt.filtered.all.vcf \
        -L $INTERVAL \
        -sn ${SAMPLE} \
        --excludeFiltered \
        --excludeNonVariants \
        -o ${INDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.all.vcf 2>> ~/${PROJECT}/logs/main_$TODAY.log
    
    GenomeAnalysisTK \
        -T SelectVariants \
        -R $REFERENCE \
        -V ${INDIR}/${PROJECT}_${INTERVAL}.ugt.filtered.all.vcf \
        -L $INTERVAL \
        -sn ${SAMPLE} \
        --excludeFiltered \
        --excludeNonVariants \
        -selectType SNP \
        -o ${INDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.snps.vcf 2>> ~/${PROJECT}/logs/main_$TODAY.log
    
    GenomeAnalysisTK \
        -T FastaAlternateReferenceMaker \
        -R ${REFERENCE} \
        -V ${INDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.all.vcf \
        -L ${INTERVAL} \
        -o ${OUTDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.all.fasta 2>> ~/${PROJECT}/logs/main_$TODAY.log
    
    sed -i "s/1/${INTERVAL}_${SAMPLE}/" ${OUTDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.all.fasta

    GenomeAnalysisTK \
        -T FastaAlternateReferenceMaker \
        -R ${REFERENCE} \
        -V ${INDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.snps.vcf \
        -L ${INTERVAL} \
        -o ${OUTDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.snps.fasta 2>> ~/${PROJECT}/logs/main_$TODAY.log

    sed -i "s/1/${INTERVAL}_${SAMPLE}/" ${OUTDIR}/${PROJECT}_${INTERVAL}_${SAMPLE}.ugt.filtered.snps.fasta

}

export -f run_consensus

parallel -j ${CORES} run_consensus ::: ${SAMPLES}

