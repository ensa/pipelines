#/bin/bash

#***************************************************************#
#                            mitobim.sh                         #
#                  written by Kerensa McElroy                   #
#                          July 28, 2015                        #
#                                                               #
#       assemble full mitochondrial genomes using mitobim       #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=${WORK}/analysis/$PROJECT/mitobim
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples
export REFERENCE=${MITO}${REF}
export SUB_READS=22750000

#---------------------------------------------------------------#

module add mitobim/1.8  
module add mira/4.0.2
module add samtools/0.1.19
module add parallel/20141122

mkdir -p ${INDIR}/MT
mkdir -p $OUTDIR

cat ~/scripts/mitobim.sh >> ~/${PROJECT}/logs/main_${TODAY}.log
echo '' >> ~/${PROJECT}/logs/main_${TODAY}.log

if [ "$REF" == '' ]
then
    SPECIES=`grep '' $SAMPLES | cut -f6 | sort -u`
    for species in $SPECIES; do samtools faidx $WORK/data/$PROJECT/*${species}*; done
fi


function my_mito {
    SAMPLE=$1
    READS=${INDIR}/MT/interleaved_${SAMPLE}_sub${SUB_READS}.fastq
    if [ ! -f $READS ] 
    then
        files=`grep "${SAMPLE}" ${SAMPLES} | cut -f1`
        echo $files | xargs -n 2 sh -c 'interleave.py ${INDIR}/${1%%${UNIQUE%.*}*}*_p.fq ${INDIR}/${2%%{UNIQUE%.*}*}*_p.fq > ${INDIR}/MT/${0}_${1%%${UNIQUE%.*}*}_R2_interleaved.fq' ${SAMPLE}
        cat ${INDIR}/MT/${SAMPLE}* > ${INDIR}/MT/interleaved_${SAMPLE}.fastq && rm ${INDIR}/MT/${SAMPLE}*
        head -"$((${SUB_READS}*8))" ${INDIR}/MT/interleaved_${SAMPLE}.fastq > ${READS} && rm ${INDIR}/MT/interleaved_${SAMPLE}.fastq
    fi
    if [ "$REF" == '' ]
    then
        ref=`grep "${SAMPLE}" ${SAMPLES} | cut -f6 | sort -u`
    else
        ref=$REF 
    fi
    rm -r ${OUTDIR}/${ref%.*}/${SAMPLE}
    mkdir -p ${OUTDIR}/${ref%.*}/${SAMPLE}
    cd ${OUTDIR}/${ref%.*}/${SAMPLE}

    cmd="MITObim -end 120 --kbait $KBAIT --missmatch $MISS -sample $SAMPLE -ref ${ref%%.*} -readpool ${READS} --quick $WORK/data/$PROJECT/${MITO}*${ref}"
    echo $cmd > ${OUTDIR}/${ref%.*}/${SAMPLE}/mitobim_${TODAY}.log
    $cmd >> ${OUTDIR}/${ref%.*}/${SAMPLE}/mitobim_${TODAY}.log 2>&1
    rm -r /tmp/${SAMPLE}*
}

export -f my_mito

echo $SAMP_NAMES
parallel -j $CORES my_mito ::: $SAMP_NAMES

