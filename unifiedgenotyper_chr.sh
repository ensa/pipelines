#!/bin/bash

#***************************************************************#
#                       unifiedgenotyper.sh                     #
#                  written by Kerensa McElroy                   #
#                          June 22, 2015                        #
#                                                               #
#           runs gatk's unified genotyper on bam files          #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/bwa
export OUTDIR=$WORK/analysis/$PROJECT/gatk
export REFERENCE=$WORK/data/$PROJECT/$REF

#----------------------------------------------------------------#

module load parallel
module load gatk/3.3.0
module load jdk

export REGION=autosomal
export CHROMS='10 11 12 13 14 15 16 17 18 19 1A 1B 20 21 22 23 24 25 26 27 28 4 4A 5 6 7 8 9 LG2 LG5 4_random 8_random 13_random 5_random 6_random 21_random 2_random 26_random 3_random 1_random LGE22 22_random 1A_random 7_random 10_random 23_random 18_random 25_random LGE22_random 8_random 15_random 12_random 20_random 11_random 4A_random 14_random 17_random 27_random 19_random 28_random 16_random 24_random 1B_random 2 3 Un' 

if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi

mkdir -p $OUTDIR/$REGION/vcfs

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log
GenomeAnalysisTK --version >> ~/$PROJECT/logs/main_$TODAY.log

ls ${INDIR}/${REGION}/${SUBSET}*.bam > ${OUTDIR}/${REGION}/vcfs/${PROJECT}_${REGION}_bam.list

function run_ugt {
    java -Xmx10g -jar /apps/gatk/3.3.0/GenomeAnalysisTK.jar \
        -T UnifiedGenotyper \
        -R $REFERENCE \
        -I ${OUTDIR}/${REGION}/vcfs/${PROJECT}_${REGION}_bam.list \
        -ploidy $PLOIDY \
        -o $OUTDIR/$REGION/vcfs/${PROJECT}_$1.ugt.raw.vcf \
        -L $1 \
        -glm SNP \
        --max_alternate_alleles 10 2>> ~/$PROJECT/logs/main_$TODAY.log
}

export -f run_ugt 

parallel -j ${CORES} run_ugt ::: $CHROMS
