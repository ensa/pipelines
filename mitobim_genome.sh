#/bin/bash

#***************************************************************#
#                        mitobim_genome.sh                      #
#                  written by Kerensa McElroy                   #
#                          July 28, 2015                        #
#                                                               #
#       assemble full mitochondrial genomes using mitobim       #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed
export OUTDIR=${WORK}/analysis/$PROJECT/assembly
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module add mitobim/1.8  
module add mira/4.0.2
module add samtools/0.1.19
module add parallel/20141122

mkdir -p ${INDIR}/GA
mkdir -p $OUTDIR

cat ~/scripts/mitobim_genome.sh >> ~/${PROJECT}/logs/main_${TODAY}.log
echo '' >> ~/${PROJECT}/logs/main_${TODAY}.log


function my_assembly {
    SPECIES=$1
    READS=${INDIR}/GA/interleaved_${SPECIES}.fastq
    if [ ! -f $READS ] 
    then
        files=`grep "${SPECIES}" ${SAMPLES} | cut -f1`
        echo $files | xargs -n 2 sh -c 'interleave.py ${INDIR}/${1%%${UNIQUE%.*}*}*_p.fq ${INDIR}/${2%%{UNIQUE%.*}*}*_p.fq > ${INDIR}/GA/${0}_${1%%${UNIQUE%.*}*}_R2_interleaved.fq' ${SPECIES}
        cat ${INDIR}/GA/${SPECIES}* > ${INDIR}/GA/interleaved_${SPECIES}.fastq && rm ${INDIR}/GA/{$SPECIES}*
    fi

    rm -r ${OUTDIR}/${REF%.*}/${SPECIES}
    mkdir -p ${OUTDIR}/${REF%.*}/${SPECIES}
    cd ${OUTDIR}/${REF%.*}/${SPECIES}

    cmd="MITObim -end 500 --kbait $KBAIT --missmatch $MISS -sample $SPECIES -ref ${REF%%.*} -readpool ${READS} --quick $WORK/data/$PROJECT/${MITO}*${REF}"
    echo $cmd > ${OUTDIR}/${REF%.*}/${SPECIES}/mitobim_${TODAY}.log
    $cmd >> ${OUTDIR}/${REF%.*}/${SPECIES}/mitobim_${TODAY}.log 2>&1
    rm -r /tmp/${SPECIES}*
}

export -f my_assembly

parallel -j 1 my_assembly ::: $SPECIES

