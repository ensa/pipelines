#!/bin/bash

#***************************************************************#
#                      sam_consensus.sh                         #
#                  written by Kerensa McElroy                   #
#                       March 30, 2016                          #
#                                                               #
#       use samtools / bcftools to make consensus from bam      #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/mitobim/${REF%.*}
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples
export REFERENCE=$WORK/data/$PROJECT/$REF
#---------------------------------------------------------------#

module add samtools/0.1.19
module add parallel/20141122

samtools | head -4 >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

cat ~/scripts/sam_consensus.sh >> ~/$PROJECT/logs/main_${TODAY}.log
echo '' >> ~/$PROJECT/logs/main_${TODAY}.log

function run_cons {
    sample=$1
    files=`grep "${sample}" ${SAMPLES} | cut -f1 | sed "s/$UNIQUE/*bam/" | sed "s/^/*/" | sort -u`

    cd $INDIR/${sample}
    echo ${sample} ${PLOIDY} > ${sample}.txt

    samtools mpileup -ug -q 50 -Q 15 --ff 3840 -f ${REFERENCE} ${files} | bcftools view -cg -s ${sample}.txt - | ~/bin/vcfutils.pl vcf2fq > ${sample}.fq
    fastqToFasta.py ${sample}.fq -l 16824 -w 60
}

export -f run_cons

parallel -j ${CORES} run_cons ::: ${SAMP_NAMES}

