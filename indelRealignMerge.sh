#!/bin/bash

#***************************************************************#
#                         indelRealignMerge.sh                  #
#                  written by Kerensa McElroy                   #
#                        September 14th, 2015                   #
#                                                               #
#               runs gatk pipeline on bam files                 #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/$ALIGNER/${REF%.*}
export OUTDIR=$WORK/analysis/$PROJECT/gatk/${REF%.*}
export REFERENCE=$WORK/data/$PROJECT/$REF

#----------------------------------------------------------------#

module add parallel
module add gatk/3.4.46
module add samtools/0.1.19

export REGION=autosomal

if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi
    
mkdir -p $OUTDIR/$REGION/intervals

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log

java -Xmx1g -jar /apps/gatk/3.4.46/GenomeAnalysisTK.jar --version >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/indelRealign.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

function run_indel {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/.bam//"`

    java -Xmx10g -jar /apps/gatk/3.4.46/GenomeAnalysisTK.jar \
        -T RealignerTargetCreator \
        -R $REFERENCE \
        -I $file \
        -o ${OUTDIR}/${REGION}/intervals/${STEM}_targetIntervals_${REGION}_merge.list \
        -L $INTERVAL \
        -dcov $DOWNSAMPLE 2> ${OUTDIR}/${REGION}/intervals/${STEM}_RTC_merge.log 

    java -Xmx10g -jar /apps/gatk/3.4.46/GenomeAnalysisTK.jar \
        -T IndelRealigner \
        -R $REFERENCE \
        -I $file \
        -targetIntervals $OUTDIR/$REGION/intervals/${STEM}_targetIntervals_${REGION}_merge.list \
        -o $INDIR/${REGION}/${STEM}_realigned.bam \
        -L $INTERVAL \
        -dcov $DOWNSAMPLE 2> ${OUTDIR}/${REGION}/intervals/${STEM}_IR_merge.log
    samtools index ${STEM}_realigned.bam
}

export -f run_indel 

ls ${INDIR}/${REGION}/${SUBSET}*merge.bam| parallel -j ${CORES} run_indel
