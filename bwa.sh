#!/bin/bash

#***************************************************************#
#                            bwa.sh                             #
#                  written by Kerensa McElroy                   #
#                       January 20, 2015                        #
#                                                               #
#                 run bwa on trimmed fastq files                #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/trimmed/
export OUTDIR=$WORK/analysis/$PROJECT/bwa/${REF%.*}
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#---------------------------------------------------------------#

module load bwa
module load parallel/20141122

mkdir -p $OUTDIR

bwa | head -5 >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

cat ~/scripts/bwa.sh >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >>  ~/$PROJECT/logs/main_$TODAY.log

# make reference index files 

#bwa index -a bwtsw -p $WORK/data/$PROJECT/${REF%.*}  $WORK/data/$PROJECT/$REF \
#    2>> ~/$PROJECT/logs/main_$TODAY.log
#

# run bwa on all trimmed fastq files

function run_bwa {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/${UNIQUE}//"`
    SAMPLE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f2`
    SPECIES=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f6`
    LIBRARY=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f3` 
    CENTRE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f4`
    SEQDATE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f5`
    UNIT=`gzip -cd $WORK/data/$PROJECT/$file | head -1 | cut -d':' -f${UNIT_RX}` #`head -1 $WORK/data/$PROJECT/$file | cut -d':' -f${UNIT_RX}` #`gzip -cd $file | head -1 | cut -d':' -f${UNIT_RX}`    
    ID=`echo ${UNIT:1}_${SAMPLE}_${SPECIES} | sed s/:/_/g`
    echo "@RG\tID:${ID}\tCN:${CENTRE}\t"` \
           `"DT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\t"` \
           `"PL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" > $OUTDIR/${STEM}.log   
    if [ ! -f ${OUTDIR}/${STEM}.sam ]
    then
        bwa mem $WORK/data/$PROJECT/${REF%.*} ${INDIR}${STEM}*p.fq.gz \
            -t $THREADS \
            -k $SEED \
            -w $WIDTH \
            -r $INTERNAL \
            -T $SCORE \
            -M \
            -R "@RG\tID:${ID}\tCN:${CENTRE}\tDT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\tPL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}" > ${OUTDIR}/${STEM}.sam 2>> $OUTDIR/${STEM}.log
    fi
}

export -f run_bwa 

parallel -j ${CORES} run_bwa ::: ${FILES}
