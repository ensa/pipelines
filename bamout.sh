#!/bin/bash

#***************************************************************#
#                            bamout.sh                          #
#                  written by Kerensa McElroy                   #
#                          June 19, 2015                        #
#                                                               #
#       bam file from haplotype caller output for debugging     #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/analysis/$PROJECT/bwa
export OUTDIR=$WORK/analysis/$PROJECT/gatk
export REFERENCE=$WORK/data/$PROJECT/$REF

#----------------------------------------------------------------#

module load parallel
module load gatk/3.3.0

mkdir -p $OUTDIR/$INTERVAL/bamout

echo 'GATK version:' >> ~/$PROJECT/logs/main_$TODAY.log
GenomeAnalysisTK --version >> ~/$PROJECT/logs/main_$TODAY.log

function run_gatk {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/.bam//"`

    GenomeAnalysisTK \
        -T HaplotypeCaller \
        -R $REFERENCE \
        -I $INDIR/${INTERVAL}/${STEM}_realigned_${INTERVAL}.bam \
        -ploidy $PLOIDY \
        --emitRefConfidence GVCF \
        --variant_index_type LINEAR \
        --variant_index_parameter 128000 \
        -o $OUTDIR/$INTERVAL/gVCFs/${STEM}.raw.snps.indels.g.vcf \
        -L $INTERVAL \
        -bamout $OUTDIR/$INTERVAL/bamout/${STEM}_realigned_${INTERVAL}_hc.bam \
        -dcov $DOWNSAMPLE 2>> ~/$PROJECT/logs/main_$TODAY.log
}

export -f run_gatk 

ls ${INDIR}/*MDUP.bam| parallel -j ${CORES} run_gatk
