#!/bin/bash

#***************************************************************#
#                            novo.sh                            #
#                  written by Kerensa McElroy                   #
#                          June 2, 2015                         #
#                                                               #
#               runs novoalign on raw fastq files               #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export INDIR=$WORK/data/$PROJECT
export OUTDIR=$WORK/analysis/$PROJECT/novoalign
export SAMPLES=$WORK/data/$PROJECT/$PROJECT.samples

#----------------------------------------------------------------#

module load parallel
module load samtools

mkdir -p $OUTDIR


#make reference index files

novoindex | grep ^# >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log

export REF_STEM=${REF%%.*} 

novoindex -t $CORES $MASKING ${INDIR}/${REF_STEM}.ndx ${INDIR}/${REF} \
    >> ~/$PROJECT/logs/main_$TODAY.log


#run novoalign on all input files

novoalign | head -1 >> ~/$PROJECT/logs/main_$TODAY.log
echo '' >> ~/$PROJECT/logs/main_$TODAY.log


function run_novoalign {
    file=$1
    NAME=$(basename "$file")
    STEM=`echo $NAME|sed "s/${UNIQUE}//"`
    SAMPLE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f2`
    LIBRARY=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f3`
    CENTRE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f4`
    SEQDATE=`grep "${STEM}*${READ_ONE}" $SAMPLES | cut -f5`
#    UNIT=`gzip -cd $file | head -1 | cut -d':' -f${UNIT_RX}`
    UNIT=`head -1 $file | cut -d':' -f${UNIT_RX}`
    ID=`echo ${UNIT:1}_${SAMPLE} | sed s/:/_/g`

    CMD=`echo novoalign \
        -d ${INDIR}/${REF_STEM}.ndx \
        -f ${INDIR}/${IN_PATH}/${STEM}* \
        -F ${CASAVA} \
        -c 1 \
        --ILQ_QC \
        --softclip 40 \
        -o SAM "@RG\tID:${ID}\tCN:${CENTRE}\tDT:${SEQDATE}\tLB:${SAMPLE}_${LIBRARY}\tPL:${PLATFORM}\tPU:${UNIT:1}\tSM:${SAMPLE}"`

    echo -e "\n$CMD\n" > ${OUTDIR}/${STEM}_$TODAY.log
    $CMD > ${OUTDIR}/${STEM}.sam 2>> ${OUTDIR}/${STEM}_$TODAY.log
}

export -f run_novoalign
ls ${INDIR}/${IN_PATH}/${SUBSET}*${READ_ONE}*${EXT} | parallel -j ${CORES} run_novoalign
