#!/bin/bash

#***************************************************************#
#                            filter.sh                          #
#                  written by Kerensa McElroy                   #
#                          June 19, 2015                        #
#                                                               #
#                     hard filter variants                      #
#***************************************************************#

#------------------------project variables-----------------------#
cd ~/working
export WORK=`pwd -P`
export OUTDIR=$WORK/analysis/${PROJECT}/gatk/${REF%.*}/all
export REFERENCE=$WORK/data/${PROJECT}/$REF

#----------------------------------------------------------------#

module load jdk

#export REGION=autosomal
#
#if [ $INTERVAL != ~/${PROJECT}/autosomal.list ]; then export REGION=$INTERVAL; fi
#
#echo 'GATK version:' >> ~/${PROJECT}/logs/main_$TODAY.log
#GenomeAnalysisTK --version >> ~/${PROJECT}/logs/main_$TODAY.log

export REGION=all


java -Xmx100g -jar ~/bin/GenomeAnalysisTK.jar \
    -T SelectVariants \
    -R $REFERENCE \
    -V $OUTDIR/${PROJECT}.${REGION}.gvcf.raw.vcf \
    -XL $EXCLUDE \
    -selectType SNP \
    -o $OUTDIR/${PROJECT}.${REGION}.gvcf.raw.snps.vcf 2>> ~/${PROJECT}/logs/main_$TODAY.log

java -Xmx100g -jar ~/bin/GenomeAnalysisTK.jar\
    -T SelectVariants \
    -R $REFERENCE \
    -V $OUTDIR/${PROJECT}.${REGION}.gvcf.raw.vcf \
#    -L $INTERVAL \
    -XL $EXCLUDE \
    -selectType INDEL \
    -o $OUTDIR/${PROJECT}.${REGION}.gvcf.raw.indels.vcf 2>> ~/${PROJECT}/logs/main_$TODAY.log

java -Xmx100g -jar ~/bin/GenomeAnalysisTK.jar \
    -T VariantFiltration \
    -R $REFERENCE \
    -V $OUTDIR/${PROJECT}.${REGION}.gvcf.raw.snps.vcf \
    -XL $EXCLUDE \
    --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || SOR > 4.0 || ReadPosRankSum < -8.0" \
    --filterName "snps_default" \
    -o $OUTDIR/${PROJECT}.${REGION}.gvcf.filtered.snps.vcf 2>> ~/${PROJECT}/logs/main_$TODAY.log

java -Xmx100g -jar ~/bin/GenomeAnalysisTK.jar \
    -T VariantFiltration \
    -R $REFERENCE \
    -V $OUTDIR/${PROJECT}.${REGION}.gvcf.raw.indels.vcf \
#    -L $INTERVAL \
    -XL $EXCLUDE \
    --filterExpression "QD < 2.0 || FS > 200.0 || SOR > 10.0" \
    --filterName "indels_default" \
    -o $OUTDIR/${PROJECT}.${REGION}.gvcf.filtered.indels.vcf 2>> ~/${PROJECT}/logs/main_$TODAY.log

java -Xmx100g -jar ~/bin/GenomeAnalysisTK.jar \
    -T CombineVariants \
    -R $REFERENCE \
    --variant:snps $OUTDIR/${PROJECT}.${REGION}.gvcf.filtered.snps.vcf \
    --variant:indels $OUTDIR/${PROJECT}.${REGION}.gvcf.filtered.indels.vcf \
    -genotypeMergeOptions PRIORITIZE \
    -priority snps,indels \
    -o $OUTDIR/${PROJECT}_${REGION}.gvcf.filtered.all.vcf \
#    -L $INTERVAL 
    -XL $EXCLUDE
